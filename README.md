# Spletni barvni stroboskop

1\. domača naloga pri predmetu [Osnove informacijskih sistemov](https://ucilnica.fri.uni-lj.si/course/view.php?id=54) (navodila)


## 6.1 Opis naloge in navodila

Na Bitbucket je na voljo javni repozitorij [https://bitbucket.org/asistent/stroboskop](https://bitbucket.org/asistent/stroboskop), ki vsebuje nedelujoč spletni stroboskop. V okviru domače naloge ustvarite kopijo repozitorija ter popravite in dopolnite obstoječo implementacijo spletne strani tako, da bo končna aplikacija z vsemi delujočimi funkcionalnostimi izgledala kot na spodnji sliki. Natančna navodila si preberite na [spletni učilnici](https://ucilnica.fri.uni-lj.si/mod/workshop/view.php?id=19181).

![Stroboskop](stroboskop.gif)

## 6.2 Vzpostavitev repozitorija

Naloga 6.2.2:

```
git clone https://WAgent47@bitbucket.org/WAgent47/stroboskop.git

```

Naloga 6.2.3:
((UVELJAVITEV))
https://bitbucket.org/WAgent47/stroboskop/commits/ea3702e4b5c7a499d4a33823ce9bd77260fe9eb6

## 6.3 Skladnja strani in stilska preobrazba

Naloga 6.3.1:
((UVELJAVITEV))
https://bitbucket.org/WAgent47/stroboskop/commits/2925eb2df9647e621ae963fefd8b2b43d2364d0e

Naloga 6.3.2:
((UVELJAVITEV))
https://bitbucket.org/WAgent47/stroboskop/commits/4d366b73b5e21166a80baebb6e1f5255a21d800b

Naloga 6.3.3:
((UVELJAVITEV))
https://bitbucket.org/WAgent47/stroboskop/commits/c533bc7bf55e9a3637b48042b3126eb76d8ae5b1

Naloga 6.3.4:
((UVELJAVITEV))
https://bitbucket.org/WAgent47/stroboskop/commits/44c84d893e1b67a080957b1c26777893cc41624d

Naloga 6.3.5:

```
((UKAZI GIT))
git checkout master
git merge izgled
git add *
git commit -m "oddaja 6.3.5"
git push origin master
```

## 6.4 Dinamika in animacija na strani

Naloga 6.4.1:
((UVELJAVITEV))
https://bitbucket.org/WAgent47/stroboskop/commits/2b79f16af0a6160f6c08f947f29f92aa0485f45c

Naloga 6.4.2:
((UVELJAVITEV))
https://bitbucket.org/WAgent47/stroboskop/commits/76dcaa47305f8b465798de3b618413f404086861

Naloga 6.4.3:
((UVELJAVITEV))
https://bitbucket.org/WAgent47/stroboskop/commits/bf6acf25e202fefc8565b8b876074bfd378a5e15

Naloga 6.4.4:
((UVELJAVITEV))
https://bitbucket.org/WAgent47/stroboskop/commits/4b8d1117e14638afdca3c35fbb81bad2b70bd5bf